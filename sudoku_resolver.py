#Sudoku_resolver
#Solves any sudoku puzzle given in a 9x9 matrix (empty places are replaced with 0's)
#By F.Fernandez

import pprint

def solve(board):
    #Main method for the resolution
    find = find_empty(board)
    #Given all the empty places, while the matrix present at least an empty place, this method will try to solve it
    while len(find)>0:
            
        for i in range(len(find)):
            #defines coordinates
            row = find[i][0]
            col = find[i][1]
            #search for available numbers for the given place
            board[row][col] = availableNumbers(board,row,col)
        #pp.pprint(board) develop tool

        #Error handler
        #if the next matrix of empty places is the same as the previous, the script fails, because is an unsolvable matrix
        if find==find_empty(board):
            raise Exception("Unsolvable board")
        else:
            #if not, declare the next matrix of empty places
            find = find_empty(board)

    return board

def find_empty(board):
    #Finds and returns all the coordinates of empty values in the matrix in [x,y] format
    allEmptyValues=[]
    #Goes over rows
    for i in range(len(board)):
        #and over columns
        for j in range(len(board[i])):
            #and adds it if empty (zero)
            if board[i][j]==0:
                allEmptyValues.append([i, j])
    return allEmptyValues

def availableNumbers(board,row,col):
    #This method search for posible numbers that correspond to a certain place
    column=[]
    possibleResults=[]

    #First defines the present values of the column involved
    for i in range(len(board)):
        column.append(board[i][col])

    #Second defines the present values of the box involved (3x3)
    box = boxoftheCell(board,row,col)

    #Third evaluate numbers in row, column and box, and adds all the possibles solutions in an array
    for i in range(1,10):
        if i not in board[row] and i not in column and i not in box:
            possibleResults.append(i)

    #Finaly, if only one resul is possible, then returns it
    if len(possibleResults)==1:
        return possibleResults[0]
    #Else, the place remains empty until more imformation is given
    else:
        return 0

def boxoftheCell(board,row,col):
    #defining the box is complex since it's positions are fixed, so:
    box=[]

    for i in quadrantDetermination(row):
        for j in quadrantDetermination(col):
            box.append(board[i][j])

    return box

def quadrantDetermination(n):
    #given the coordinates of the place evaluated determines witch quadrant belongs to
    if n <= 2:
        return range(0,2)
    elif n <= 5:
        return range(3,5)
    else:
        return range(6,8)

def print_board(board):
    #Finaly, use PrettyPrint to the output, this can be replaced to complete an actual sudoku. TBI.
    for i in range(len(board)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - - - -")
        for j in range(len(board[i])):
            if j % 3 == 0:
                print(" | ",end="")
            
            if j == 8:
                print(board[i][j], end="\n")
            else:
                print(str(board[i][j]) + " ", end="")


board = [
[0,0,3,6,8,0,4,0,9],
[0,0,8,0,0,0,6,3,0],
[0,0,4,5,0,0,7,0,2],
[8,0,0,4,2,0,3,7,5],
[6,3,0,0,0,8,9,2,0],
[0,0,2,0,9,0,8,0,6],
[0,0,5,0,4,7,1,0,0],
[0,0,0,0,0,5,2,0,8],
[4,0,6,3,1,2,5,9,7]
]

pp = pprint.PrettyPrinter(width=41,compact=True)
solve(board)
print("FINAL Board:", end="\n")
pp.pprint(board)
